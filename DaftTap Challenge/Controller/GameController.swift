//
//  GameController.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 11.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class GameController: UIViewController {
    @IBOutlet private weak var timeCounterLabel: UILabel!
    @IBOutlet private weak var tapCounterLabel: UILabel!
    @IBOutlet private weak var countingDownLabel: UILabel!
    
    @IBOutlet private var tapGestureRecognizer: UITapGestureRecognizer!
    
    @IBOutlet weak var countingDownLabelXConstraint: NSLayoutConstraint!
    @IBOutlet weak var countingDownLabelYConstraint: NSLayoutConstraint!
    
    var mainViewController: MainViewController!
    var gameLogic: GameLogic!
    
    private var countingDownTimer: Timer!
    private var mainTimer: Timer!
    private var backgroundColorChangerThread: Timer!
    
    override func viewDidAppear(_ animated: Bool) {
        gameLogic.gameOverAction = { [unowned self] in self.gameOver() }
        tapGestureRecognizer.isEnabled = false
        
        if gameLogic.isThisAFirstGame() {
            showGameRules()
        } else {
            startCountingDown()
        }
    }
    
    private func showGameRules() {
        let alert = UIAlertController(
            title: "Rules",
            message: "You have 5 sec to make as many taps as you can. You can tap all over the view. Good luck!",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "Lets play!", style: .default) { action in self.startCountingDown() })
    
        present(alert, animated: true)
    }
    
    private func startCountingDown() {
        let strings = ["..2..", "...1", "PLAY!", ""]
        var index = 0
        
        animateCountingDownLabel()
        countingDownTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.countingDownLabel.text = strings[index]
            index += 1
            
            self.animateCountingDownLabel()
            
            if index == strings.count - 1 {
                self.tapGestureRecognizer.isEnabled = true
                self.gameLogic.startANewGame()
                self.startMainTimer()
            } else if index == strings.count {
                self.countingDownTimer.invalidate()
            }
        }
    }
    
    private func animateCountingDownLabel() {
        countingDownLabelXConstraint.constant -= 105
        countingDownLabelYConstraint.constant += 20
        countingDownLabel.alpha = 0
        
        UIView.animate(withDuration: 1.0) {
            self.countingDownLabelXConstraint.constant += 133
            self.countingDownLabel.alpha = 1
        }
    }
    
    private func startMainTimer() {
        mainTimer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { timer in
            let timeToGo = self.gameLogic.getNewGameTime()
            self.setTimerLabel(timeToGo: timeToGo)
        }
    }
    
    private func setTimerLabel(timeToGo: Decimal) {
        let floatValue = Float(truncating: timeToGo as NSNumber)
        
        DispatchQueue.main.async {
            self.timeCounterLabel.text = String(format: "%0.3f", floatValue)
        }
    }
    
    private func gameOver() {
        mainTimer.invalidate()
       
        let completion = { [unowned self] (score: (Int,Date)) -> Int? in
            self.mainViewController.addNewScoreAndReturnPosition(score: score)
        }
        
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: "Results",
                message: self.gameLogic.getAlertMessage(completion: completion),
                preferredStyle: .alert
            )
            
            alert.addAction(UIAlertAction(title: "OK", style: .default) {
                action in self.dismiss(animated: true, completion: nil)
            })
            
            self.present(alert, animated: true)
        }
    }
    
    @IBAction private func tap(_ sender: Any) {
        let tapCount = gameLogic.getNewTapCount()
        tapCounterLabel.text = String(tapCount)
        
        view.backgroundColor = UIColor.red
        backgroundColorChangerThread?.invalidate()
        backgroundColorChangerThread = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { timer in
            if let components = self.view.backgroundColor?.cgColor.components {
                self.view.backgroundColor = UIColor.init(red: components[0], green: components[1] + 0.1, blue: components[2] + 0.1, alpha: components[3])
            }
        }
    }
}
