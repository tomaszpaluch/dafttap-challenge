//
//  ScoreboardCollectionViewCell.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 12.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class ScoreboardCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var ordinalNumberLabel: UILabel!
    @IBOutlet private weak var scoreLabel: UILabel!
    @IBOutlet private weak var dateTimeLabel: UILabel!
    
    func setInfo(oridinalNumber: Int, taps: Int, date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        
        ordinalNumberLabel.text = String("\(oridinalNumber + 1).")
        scoreLabel.text = String(taps)
        dateTimeLabel.text = dateFormatter.string(from: date)
    }
}
