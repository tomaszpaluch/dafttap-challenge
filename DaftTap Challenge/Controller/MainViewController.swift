//
//  ViewController.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 11.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    @IBOutlet weak var scoreboardCollectionView: UICollectionView!
    @IBOutlet weak var playButton: UIButton!
    
    private var gameLogic: GameLogic
    private let scoreboard: Scoreboard
    
    required init?(coder aDecoder: NSCoder) {
        gameLogic = GameLogic()
        scoreboard = Scoreboard()
        
        super.init(coder: aDecoder)
        
        scoreboard.reloadAction = { [unowned self] in self.scoreboardCollectionView.reloadData() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scoreboardCollectionView.delegate = self
        scoreboardCollectionView.dataSource = self
        
        playButton.layer.cornerRadius = 5
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? GameController {
            destinationVC.mainViewController = self
            destinationVC.gameLogic = gameLogic
        }
    }
    
    func addNewScoreAndReturnPosition(score: (Int, Date)) -> Int? {
        return scoreboard.addNewScoreAndReturnPosition(score: score)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return scoreboard.getNumberOfScores()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = scoreboardCollectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! ScoreboardCollectionViewCell
        
        let scoreInfo = scoreboard.getScoreAt(index: indexPath.row)
        cell.setInfo(oridinalNumber: indexPath.row, taps: scoreInfo.0, date: scoreInfo.1)
        
        return cell
    }
}

