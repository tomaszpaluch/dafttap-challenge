//
//  PersistnceManager.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 12.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation
import CoreData

final class PersitanceManager {
    private init() {}
    static let shared = PersitanceManager()
    lazy var context = persistentContainer.viewContext
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreDataModel")
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()
    
    func fetch() -> [SavedScore] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedScore")
        do {
            let fetchedObjects = try context.fetch(fetchRequest) as? [SavedScore]
            return fetchedObjects ?? [SavedScore]()
        } catch {
            print(error)
            return [SavedScore]()
        }
    }
    
    func delete(_ object: NSManagedObject) {
        context.delete(object)
        saveContext()
    }
    
    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
