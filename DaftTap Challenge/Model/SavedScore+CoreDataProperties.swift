//
//  SavedScore+CoreDataProperties.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 12.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//
//

import Foundation
import CoreData


extension SavedScore: Comparable {
    @NSManaged public var taps: Int16
    @NSManaged public var date: Date
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<SavedScore> {
        return NSFetchRequest<SavedScore>(entityName: "SavedScore")
    }
    
    convenience init(context: NSManagedObjectContext, taps: Int, date: Date) {
        self.init(context: context)
        
        self.taps = Int16(taps)
        self.date = date
    }
    
    public static func < (lhs: SavedScore, rhs: SavedScore) -> Bool {
        if lhs.taps == rhs.taps {
            return rhs.date > lhs.date
        } else {
            return lhs.taps > rhs.taps
        }
    }
}
