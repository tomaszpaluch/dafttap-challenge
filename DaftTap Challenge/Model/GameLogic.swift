//
//  GameLogic.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 11.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

class GameLogic {
    var gameOverAction: (() -> Void)!
    
    private var gameCounter: Int
    private var taps: Int
    private var timeToGo: Decimal
    
    private var lastGameDate: Date!
    
    init() {
        gameCounter = 0
        taps = 0
        timeToGo = 5
    }
    
    func isThisAFirstGame() -> Bool {
        return gameCounter == 0
    }
    
    func startANewGame() {
        gameCounter += 1
        taps = 0
        timeToGo = 5
        
        lastGameDate = Date()
    }
    
    func getNewGameTime() -> Decimal {
        timeToGo -= 0.01
        
        if self.timeToGo == 0 {
            gameOverAction()
        }
        
        return timeToGo
    }
    
    func getNewTapCount() -> Int {
        taps += 1
        return taps
    }
    
    func getAlertMessage(completion: @escaping ((Int, Date)) -> Int?) -> String {
        var message = ""
        if let position = completion((taps,lastGameDate)) {
            message += "Congratulations! you've earned the " + getOrdinal(position: position) + " highest score! "
        }
        message += "You've tapped \(taps) times."
        return message
    }
    
    private func getOrdinal(position: Int) -> String {
        var ending: String
        
        switch position {
        case 1:
            ending = "st"
        case 2:
            ending = "nd"
        default:
            ending = "th"
        }
        
        return "\(position)" + ending
    }
}
