//
//  Scoreboard.swift
//  DaftTap Challenge
//
//  Created by tomaszpaluch on 12.05.2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import Foundation

class Scoreboard {
    let persitanceManager: PersitanceManager
    var reloadAction: (() -> Void)!
    private var scores: [SavedScore]
    
    init() {
        persitanceManager = PersitanceManager.shared
        scores = []
        
        scores.append(contentsOf: persitanceManager.fetch())
        scores.sort()
    }
    
    func getNumberOfScores() -> Int {
        return scores.count
    }
    
    func getScoreAt(index: Int) -> (Int, Date) {
        let score = scores[index]
        return (Int(score.taps), score.date)
    }
    
    func addNewScoreAndReturnPosition(score: (Int, Date)) -> Int? {
        var lastScoreTaps: Int
        
        if scores.count < 5 {
            lastScoreTaps = 0
        } else {
            lastScoreTaps = Int(scores.last!.taps)
        }
        
        if score.0 > lastScoreTaps {
            let score = SavedScore(context: persitanceManager.context, taps: score.0, date: score.1)
            
            scores.append(score)
            scores.sort { $0.taps > $1.taps }
            
            if scores.count > 5 {
                persitanceManager.delete(scores.last!)
                scores.remove(at: scores.count - 1)
            }
            
            reloadAction()
            
            return scores.firstIndex { $0 == score }! + 1
        } else {
            return nil
        }
    }
    
}
